
public class Bubblesort {
	public static void main(String[] args) {
		int [] liste = new int[20];
		
		for(int i = 0; i < liste.length; i++) {
			liste[i] = (int) (Math.random() * 100);
		}
		
		System.out.println("Unsortiert:");
		for(int i = 0; i < liste.length; i++) {
			System.out.print(liste[i] + ", ");
		}
		
		//bubblesort
		long time = System.currentTimeMillis();
		
		boolean wurdegetauscht;
		do {
			wurdegetauscht = true;
			
			for(int i = 0; i < liste.length; i++) {
				if(i+1 < liste.length) {
					if(liste[i] > liste[i+1]) {
						int temp = liste[i];
						
						liste[i] = liste[i+1];
						liste[i+1] = temp;
						
						wurdegetauscht = false;
					}
				}
			}
		}while(!wurdegetauscht);
		
		System.out.println("\nSortiert:");
		for(int i = 0; i < liste.length; i++) {
			System.out.print(liste[i] + ", ");
		}
		
		System.out.println(" ");
		System.out.println(System.currentTimeMillis() - time + "ms");
	}
}
