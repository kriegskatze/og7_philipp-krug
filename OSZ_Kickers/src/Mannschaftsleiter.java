
public class Mannschaftsleiter extends Spieler{
	private String name_Mannschaft;
	private int rabatt;
	
	public Mannschaftsleiter() {}
	public Mannschaftsleiter(String name_Mannschaft, int rabatt, int trikotnummer, 
			String position, String name, String telefonnummer, boolean jahresbeitrag_bezahlt) {
		super(trikotnummer, position, name, telefonnummer, jahresbeitrag_bezahlt);
		this.name_Mannschaft = name_Mannschaft;
		this.rabatt = rabatt;
	}
	
	public String getName_Mannschaft() {
		return name_Mannschaft;
	}
	public void setName_Mannschaft(String name_Mannschaft) {
		this.name_Mannschaft = name_Mannschaft;
	}
	public int getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
}
