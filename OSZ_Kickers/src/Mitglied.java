
public class Mitglied {
	private String name;
	private String telefonnummer;
	private boolean jahresbeitrag_bezahlt;
	
	public Mitglied() {}
	public Mitglied(String name, String telefonnummer, boolean jahresbeitrag_bezahlt) {
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbeitrag_bezahlt = jahresbeitrag_bezahlt;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isJahresbeitrag_bezahlt() {
		return jahresbeitrag_bezahlt;
	}
	public void setJahresbeitrag_bezahlt(boolean jahresbeitrag_bezahlt) {
		this.jahresbeitrag_bezahlt = jahresbeitrag_bezahlt;
	}
	
}