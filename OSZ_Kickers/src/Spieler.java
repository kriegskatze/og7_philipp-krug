
public class Spieler extends Mitglied {
	private int trikotnummer;
	private String position;
	
	public Spieler() {}
	public Spieler(int trikotnummer, String position, String name, 
			String telefonnummer, boolean jahresbeitrag_bezahlt) {
		super(name, telefonnummer, jahresbeitrag_bezahlt);
		this.trikotnummer = trikotnummer;
		this.position = position;
	}
	
	public int getTrikotnummer() {
		return trikotnummer;
	}
	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
}
