
public class Trainer extends Mitglied{
	private char lizenzklasse;
	private double mAufwandentschaedigung;
	
	public Trainer() {}
	public Trainer(char lizenzklasse, double mAufwandentschaedigung, String name, 
			String telefonnummer, boolean jahresbeitrag_bezahlt) {
		super(name, telefonnummer, jahresbeitrag_bezahlt);
		this.lizenzklasse = lizenzklasse;
		this.mAufwandentschaedigung = mAufwandentschaedigung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public double getmAufwandentschaedigung() {
		return mAufwandentschaedigung;
	}

	public void setmAufwandentschaedigung(double mAufwandentschaedigung) {
		this.mAufwandentschaedigung = mAufwandentschaedigung;
	}
}
