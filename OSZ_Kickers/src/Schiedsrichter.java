
public class Schiedsrichter extends Mitglied{
	private int anzSpiele;

	public Schiedsrichter() {}
	public Schiedsrichter(int anzSpiele, String name, 
			String telefonnummer, boolean jahresbeitrag_bezahlt) {
		super(name, telefonnummer, jahresbeitrag_bezahlt);
		this.anzSpiele = anzSpiele;
	}


	public int getAnzSpiele() {
		return anzSpiele;
	}

	public void setAnzSpiele(int anzSpiele) {
		this.anzSpiele = anzSpiele;
	}
}
