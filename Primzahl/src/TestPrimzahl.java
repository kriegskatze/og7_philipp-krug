import java.util.Scanner;

public class TestPrimzahl {
	public static void main(String[] args) {
		Primzahl prim = new Primzahl();
		Stoppuhr stoptime = new Stoppuhr();
		Scanner scan = new Scanner(System.in);
		
		long testzahl = scan.nextLong();
		
		stoptime.start();
		System.out.println(prim.istPrimzahl(testzahl));
		stoptime.stop();
		System.out.println(stoptime.getMS());
		
		scan.close();
	}

}
