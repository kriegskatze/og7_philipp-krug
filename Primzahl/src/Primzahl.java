
public class Primzahl {
	
	public Primzahl() {}

	public boolean istPrimzahl(long kandidat) {
		long hilfs_zaehler;
		
		for(hilfs_zaehler = 2; hilfs_zaehler < kandidat; hilfs_zaehler++) {
			if(kandidat%hilfs_zaehler == 0) {
				return false;
			}
		}
		return true;
	}
}
