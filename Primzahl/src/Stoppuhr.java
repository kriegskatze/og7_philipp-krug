
public class Stoppuhr {
	private long start;
	private long stop;
	
	public Stoppuhr() {
	}

	public void start() {
		this.start = System.currentTimeMillis();
	}
	public void stop() {
		this.stop = System.currentTimeMillis();
	}
	public void reset() {
		start = 0;
		stop = 0;
	}
	public long getMS() {
		return stop - start;
	}
}
