package de.futurehome.tanksimulator;
public class Tank {
	
	private double fuellstand;
	private double prznt_fuellstand;

	public Tank(double fuellstand, double prznt_fuellstand) {
		this.fuellstand = fuellstand;
		this.prznt_fuellstand = prznt_fuellstand;
	}

	public double getPrznt_fuellstand() {
		return prznt_fuellstand;
	}
	
	public void setPrznt_fuellstand(double prznt_fuellstand) {
		this.prznt_fuellstand = prznt_fuellstand;
	}
	
	public double getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		this.fuellstand = fuellstand;
	}

}
