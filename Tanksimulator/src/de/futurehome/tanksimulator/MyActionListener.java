package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();
			 double prznt_fuellstand = f.myTank.getPrznt_fuellstand();
			 
			 fuellstand = fuellstand + 5;
			 prznt_fuellstand = prznt_fuellstand + 5;
			 
			 f.myTank.setFuellstand(fuellstand);
			 f.myTank.setPrznt_fuellstand(prznt_fuellstand);
			 
			 if(100 < fuellstand) {
				 f.myTank.setFuellstand(100);
				 f.myTank.setPrznt_fuellstand(100);
			 }
			 
			 f.lblFuellstand.setText(""+fuellstand);
			 f.lblProzent.setText(""+prznt_fuellstand+"%");
		}
		
		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			double prznt_fuellstand = f.myTank.getPrznt_fuellstand();
			
			fuellstand = fuellstand - 2;
			prznt_fuellstand = prznt_fuellstand - 2;
			
			f.myTank.setFuellstand(fuellstand);
			f.myTank.setPrznt_fuellstand(prznt_fuellstand);
			
			f.lblFuellstand.setText(""+fuellstand);
			 f.lblProzent.setText(""+prznt_fuellstand+"%");
		}
		if (obj == f.btnZuruecksetzen) {
			double fuellstand = f.myTank.getFuellstand();
			double prznt_fuellstand = f.myTank.getPrznt_fuellstand();
			
			fuellstand = 0;
			prznt_fuellstand = 0;
			
			f.myTank.setFuellstand(fuellstand);
			f.myTank.setPrznt_fuellstand(prznt_fuellstand);
			
			f.lblFuellstand.setText(""+fuellstand);
			f.lblFuellstand.setText(""+prznt_fuellstand+"%");
		}

	}
}