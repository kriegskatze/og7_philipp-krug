import java.util.Scanner;

public class Fibbonacci {
	public static long fibo(long n) {
		if(n == 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		} else {
			return fibo(n-1) + fibo(n-2);
		}
	}
	
	public static void main(String[] args) {	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Geben sie eim wie oft sie die Fibonaccireihe rechnen wollen:");
		System.out.println(Fibbonacci.fibo(sc.nextLong()));
		sc.close();
	}
}
