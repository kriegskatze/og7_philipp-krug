package zahlensortierer;

import java.util.*;

/**
 * @author Philipp Krug
 *
 */
public class Zahlensortierer {

	//TODO  
	// vervollständigen Sie die main methode so, dass sie 3 Zahlen vom Benutzer 
	// einliest und die kleinste und größte Zahl ausgibt.
	public static void main(String[] args) {
		

		Scanner myScanner = new Scanner(System.in);

		System.out.println("1. Zahl: ");
		int zahl1 = myScanner.nextInt();
		
		// add you code here
		
		System.out.println("2. Zahl: ");
		int zahl2 = myScanner.nextInt();
		System.out.println("3. Zahl: ");
		int zahl3 = myScanner.nextInt();
		
		if(zahl1 < zahl2 && zahl1 < zahl3) {
			if(zahl2 > zahl3) {
				System.out.println("kleinere Zahl: " + zahl1 + 
						"\ngroessere Zahl: " + zahl3);
			}else {
				System.out.println("kleinere Zahl: " + zahl1 + 
						"\ngroessere Zahl: " + zahl2);
			}
		}
		
		if(zahl2 < zahl1 && zahl2 < zahl3) {
			if(zahl1 > zahl3) {
				System.out.println("kleinere Zahl: " + zahl2 + 
						"\ngroessere Zahl: " + zahl1);
			}else {
				System.out.println("kleinere Zahl: " + zahl2 + 
						"\ngroessere Zahl: " + zahl3);
			}
		}
		
		if(zahl3 < zahl2 && zahl3 < zahl1) {
			if(zahl2 > zahl1) {
				System.out.println("kleinere Zahl: " + zahl3 + 
						"\ngroessere Zahl: " + zahl2);
			}else {
				System.out.println("kleinere Zahl: " + zahl3 + 
						"\ngroessere Zahl: " + zahl1);
			}
		}
		
		// die noetige änderung
			
		myScanner.close();
	}
}
